<?php 

require_once 'connectvars.php';

class DbAccess {

	function getPDO() {
		static $pdo;

		if (!isset($pdo)) {
			$dsn = 'mysql:host=' . DB_HOST . ';port=' . DB_PORT . ';dbname=' . DB_NAME;
			$pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);				
			$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
			$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			$pdo->exec('set session sql_mode = traditional');
			$pdo->exec('set session innodb_strict_mode = on');
		}
		return $pdo;
	}
}