<?php 
require_once 'dbaccess_class.php';

class User{

	var $username;
	var $password;
	var $hashed_pass;
	var $salt_hex;

	function __construct($username, $password){
		$this->username = trim($username);
		$this->password = trim($password);
	}

	function checkPass(){
		$db = new DbAccess;
		$pdo = $db->getPDO();
		$stmt = $pdo->prepare("SELECT user_id, hashed_pass, salt_hex FROM credentials WHERE username = :tmpUser");
		$stmt->execute(array('tmpUser' => $this->username));
		$row = $stmt->fetch();

		$passToBeChecked =  hash('whirlpool', hex2bin($row['salt_hex']) . $this->password);

			if ($passToBeChecked == $row['hashed_pass']) {
				return $row['user_id'];
			} else {
				return FALSE;
			}
	}

	function hashPass(){
		$salt_length = mt_rand(21,31);
		$salt_bin = openssl_random_pseudo_bytes ($salt_length);
		$this->salt_hex = bin2hex($salt_bin);
		$this->hashed_pass = hash('whirlpool', $salt_bin . $this->password);
	}

	function echoUser(){
		echo "User: " . $this->username . " has password: " . $this->password . " hashed with salt: " . $this->salt_hex . " to make this hashed pass: " . $this->hashed_pass;
	}

	function add_user_to_db(){
		$this->hashPass();

		$db = new DbAccess;
		$pdo = $db->getPDO();
		$stmt = $pdo->prepare("INSERT INTO credentials (username, password, salt_hex, hashed_pass, date_of_reg) VALUES (:username, :password, :salt_hex, :hashed_pass, NOW())");

		try {
		$stmt->execute(array('username' => $this->username, 'password' => $this->password, 'salt_hex' => $this->salt_hex, 'hashed_pass' => $this->hashed_pass));

		echo "User: " . $this->username . " has been inserted into DB." . "<br />";
		} 

		catch(Exception $e) {
  			echo 'Message: ' .$e->getMessage() . '<br />';
		}
	}
}
