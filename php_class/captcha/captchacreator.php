<?php 

// This script can be used as a PNG file, and it creates session data

session_start();

require_once 'captcha_class.php';

$myCaptcha = new Captcha;

$_SESSION['hashed_phrase'] = $myCaptcha->hashedPhrase();

$myCaptcha->createImage();
