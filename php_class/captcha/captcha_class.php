<?php 

class Captcha {
		
	var $CAPTCHA_NUMCHARS = 6;
	var $CAPTCHA_WIDTH = 120;
	var $CAPTCHA_HEIGHT = 34;

	var $pass_phrase = '';

	var $img;

	/**
	 * Captcha constructor.
     */
	function __construct(){
		for ($i=0; $i < $this->CAPTCHA_NUMCHARS ; $i++) { 
			$this->pass_phrase .= chr(mt_rand(97,122));
		}

		$img = imagecreatetruecolor($this->CAPTCHA_WIDTH, $this->CAPTCHA_HEIGHT);
	//	$text_color = imagecolorallocate($img, 0, 0, 0);
		$graphic_color = imagecolorallocate($img, 210, 210, 230);
		$pixel_color = imagecolorallocate($img, 183, 51, 122);
		$elipse_color = imagecolorallocate($img, 110, 210, 230);
		$bg_color = imagecolorallocate($img, 51, 122, 183);

		imagefilledrectangle($img, 0, 0, $this->CAPTCHA_WIDTH, $this->CAPTCHA_HEIGHT, $bg_color);

		for ($i=0; $i < 20 ; $i++) { 
			imagesetpixel($img, mt_rand() % $this->CAPTCHA_WIDTH, mt_rand() % $this->CAPTCHA_HEIGHT, $pixel_color);
		}

		for ($i=0; $i < 3; $i++) { 
			imageline($img, 0, mt_rand() % $this->CAPTCHA_HEIGHT, $this->CAPTCHA_WIDTH, mt_rand() % $this->CAPTCHA_HEIGHT, $graphic_color);
		}

		imageellipse($img, $this->CAPTCHA_WIDTH / 2, $this->CAPTCHA_HEIGHT / 2, $this->CAPTCHA_WIDTH, $this->CAPTCHA_HEIGHT, $elipse_color);
		imagettftext($img, 24, 0, 5, $this->CAPTCHA_HEIGHT - 7, $pixel_color, 'font/NovaMono.ttf', $this->pass_phrase);

		$this->img = $img;
	} // end construct

	function hashedPhrase(){
		return sha1($this->pass_phrase);
	}

	function createImage(){
		header('Content-Type: image/png');
		imagepng($this->img);
		imagedestroy($this->img);
	}
} // end class Captcha