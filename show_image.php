<?php 
require_once 'dbAccess_class.php';

function showImage($image_id){
	$db = new DbAccess;
	$pdo = $db->getPDO();
	$stmt = $pdo->prepare("SELECT image_filename FROM image_data WHERE image_id = :image_id");

	try {
		$stmt->execute(array('image_id' => $image_id));
		$row = $stmt->fetch();
		return $row['image_filename'];
		} 

	catch(Exception $e) {
  		echo 'Message: ' .$e->getMessage() . '<br />';
		}
} // end showImage
