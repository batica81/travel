<?php 

require_once 'user_class.php';

function rand_num_pass($numchar){
	// pravi random string od $numchar cifara
	$pass = "";
	for ($i=0; $i < $numchar; $i++) { 
		$pass .= mt_rand(0,9);
	}
	return $pass;
}


$users = [];

for ($i=0; $i < 100; $i++) { 
 
	$users[$i] = new User(('Pera' . rand_num_pass(4)), rand_num_pass(3));
	$users[$i]->add_user_to_db();
}
