-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.39 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.5005
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for travel_db
CREATE DATABASE IF NOT EXISTS `travel_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `travel_db`;


-- Dumping structure for table travel_db.credentials
CREATE TABLE IF NOT EXISTS `credentials` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `salt_hex` varchar(64) NOT NULL,
  `hashed_pass` varchar(130) NOT NULL,
  `date_of_reg` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5398 DEFAULT CHARSET=latin1;

-- Dumping data for table travel_db.credentials: ~3 rows (approximately)
/*!40000 ALTER TABLE `credentials` DISABLE KEYS */;
INSERT INTO `credentials` (`user_id`, `username`, `password`, `salt_hex`, `hashed_pass`, `date_of_reg`) VALUES
	(4563, 'Pera', '123', '4b12ecc98ba7a587329b54d8038790673abfe1944c404ddbab53fef1', '72b5adc995ee3e70d933822742fac3509542012585108d7c6b1df8328db6675dad188a97f2ece8ffb89efe58664ac3c27c2e9850c30da2039cfac06e156ad13f', '0000-00-00 00:00:00'),
	(4566, 'alen', '123', 'fd4cd9f8875af1080206eff3b37b247d4a792385e0983838bf', '57db1ebe44ddb9c03e37650a71d31d65a386c83d534659a111670ebcc88c6dd3870cbd98055ce8d28367a6b63904839abe53c43d97b1c7660641eba561176c89', '0000-00-00 00:00:00'),
	(4567, 'loki', 'kill', '083862251303fa63165679777cb7cb63be0085195330b99c356253', '11f906da8a02a6b0a72394f387cd9c483a7bf18d67cb5457ecd65b6b21fe70c9f1ee0e8cb990d5cd20b1570ff9f1b709dc426bc7e788ad959168cefc8cd65059', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `credentials` ENABLE KEYS */;


-- Dumping structure for table travel_db.image_comments
CREATE TABLE IF NOT EXISTS `image_comments` (
  `comment_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image_comment` varchar(50) NOT NULL,
  `comment_date` datetime NOT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `FK_image_comments_image_data` (`image_id`),
  KEY `FK_image_comments_credentials` (`user_id`),
  CONSTRAINT `FK_image_comments_credentials` FOREIGN KEY (`user_id`) REFERENCES `credentials` (`user_id`),
  CONSTRAINT `FK_image_comments_image_data` FOREIGN KEY (`image_id`) REFERENCES `image_data` (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table travel_db.image_comments: ~0 rows (approximately)
/*!40000 ALTER TABLE `image_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `image_comments` ENABLE KEYS */;


-- Dumping structure for table travel_db.image_data
CREATE TABLE IF NOT EXISTS `image_data` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `image_description` varchar(50) DEFAULT NULL,
  `image_filename` varchar(50) DEFAULT NULL,
  `image_post_date` datetime DEFAULT NULL,
  PRIMARY KEY (`image_id`),
  UNIQUE KEY `image_id` (`image_id`),
  KEY `FK_image_data_credentials` (`user_id`),
  CONSTRAINT `FK_image_data_credentials` FOREIGN KEY (`user_id`) REFERENCES `credentials` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table travel_db.image_data: ~3 rows (approximately)
/*!40000 ALTER TABLE `image_data` DISABLE KEYS */;
INSERT INTO `image_data` (`image_id`, `user_id`, `image_description`, `image_filename`, `image_post_date`) VALUES
	(2, 4567, 'sfsd', './images/1447348951aaa.jpg', '2015-11-12 18:22:31'),
	(3, 4567, 'fgdgd', './images/1447349004aaa.jpg', '2015-11-12 18:23:24'),
	(4, 4567, '88', './images/1447349721url.jpg', '2015-11-12 18:35:21');
/*!40000 ALTER TABLE `image_data` ENABLE KEYS */;


-- Dumping structure for table travel_db.travel_details
CREATE TABLE IF NOT EXISTS `travel_details` (
  `travel_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `travel_description` varchar(50) NOT NULL,
  `image_01` varchar(50) NOT NULL,
  `image_02` varchar(50) NOT NULL,
  `image_03` varchar(50) NOT NULL,
  `travel_post_date` datetime NOT NULL,
  PRIMARY KEY (`travel_id`),
  KEY `FK_travel_details_credentials` (`user_id`),
  CONSTRAINT `FK_travel_details_credentials` FOREIGN KEY (`user_id`) REFERENCES `credentials` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table travel_db.travel_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `travel_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `travel_details` ENABLE KEYS */;


-- Dumping structure for table travel_db.user_details
CREATE TABLE IF NOT EXISTS `user_details` (
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `bio` text,
  KEY `FK__credentials` (`user_id`),
  CONSTRAINT `FK__credentials` FOREIGN KEY (`user_id`) REFERENCES `credentials` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table travel_db.user_details: ~2 rows (approximately)
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;
INSERT INTO `user_details` (`user_id`, `email`, `dob`, `first_name`, `last_name`, `bio`) VALUES
	(NULL, '1', NULL, '2', '3', '4'),
	(NULL, '1', NULL, '2', '3', '4');
/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;


-- Dumping structure for table travel_db.user_profile
CREATE TABLE IF NOT EXISTS `user_profile` (
  `profile_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `travel_01` int(11) NOT NULL,
  `travel_02` int(11) NOT NULL,
  `travel_03` int(11) NOT NULL,
  PRIMARY KEY (`profile_id`),
  KEY `FK_user_profile_credentials` (`user_id`),
  CONSTRAINT `FK_user_profile_credentials` FOREIGN KEY (`user_id`) REFERENCES `credentials` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table travel_db.user_profile: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_profile` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
