<?php 

require_once ('header.php');
require_once ('php_class/user_class.php');

if (!empty($_SESSION['user_id'])) {
    echo "You are logged in as: " . $_SESSION['username'];
    echo ' <a href="userpage.php">Home page</a>';
    die();
}

if (isset($_POST['submit']) && (!empty($_POST['username'])) && (!empty($_POST['password']))) {

    // if (isset($_POST['verify']) && ($_SESSION['hashed_phrase'] == sha1($_POST['verify']))){

     if (isset($_POST['verify'])){

        $user01 = new User($_POST['username'], $_POST['password']);
        $user01->add_user_to_db();
      } else {echo "Bad captcha!";}
}


?>
      <h2>Registration Form</h2>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" class="form-horizontal col-md-6" role="form" >

      <div class="form-group">
        <label for="username" class="col-sm-3 control-label">Username</label>
        <div class="col-sm-9">
          <input name="username" type="text" id="username" placeholder="Username" class="form-control" autofocus>
        </div>
      </div>

      <div class="form-group">
        <label for="password" class="col-sm-3 control-label">Password</label>
        <div class="col-sm-9">
          <input name="password" type="password" id="password" placeholder="Password" class="form-control">
        </div>
      </div>

      <div class="form-group">
        <label for="firstName" class="col-sm-3 control-label">Full Name</label>
        <div class="col-sm-9">
          <input name="firstName" type="text" id="firstName" placeholder="Full Name" class="form-control" autofocus>
          <!-- <span class="help-block">Last Name, First Name, eg.: Smith, Harry</span> -->
        </div>
      </div>

      <div class="form-group">
        <label for="email" class="col-sm-3 control-label">Email</label>
        <div class="col-sm-9">
          <input name="email" type="email" id="email" placeholder="Email" class="form-control">
        </div>
      </div>

      <div class="form-group">
        <label for="birthDate" class="col-sm-3 control-label">Date of Birth</label>
        <div class="col-sm-9">
          <input name="birthDate" type="date" id="birthDate" class="form-control">
        </div>
      </div>
    

      <div class="form-group">

        
        <label for="verify" class="col-sm-3 control-label">Captcha verification</label>
        <div class="col-sm-9">

          <input name="verify" type="text" id="verify" class="form-control col-sm-3">
          <img class="captchaImg" src="php_class/captcha/captchacreator.php" alt="Verification passphrase">


      </div>
      </div>


      <!-- /.form-group -->

      <div class="form-group">
        <div class="col-sm-9 col-sm-offset-3">
          <button name="submit" type="submit" class="btn btn-primary btn-block" id="submit">Register</button>
        </div>
      </div>
 
     </form>
    <!-- /form -->






  



<?php 
require_once ('footer.php');
?>