<?php 
require_once ('header.php');
require_once ('php_class/user_class.php');
?>

    <h4>Please login....</h4>

<?php 

if (!empty($_SESSION['user_id'])) {
		echo "You are logged in as: <span class='username'> " . $_SESSION['username'] . "</span>";
		echo ' <a href="index.php">Home page</a>';
		die();
}

if (isset($_POST['submit']) && (!empty($_POST['username'])) && (!empty($_POST['password']))  ){

	// && ($_SESSION['hashed_phrase'] == sha1($_POST['verify']))) {


		$username = $_POST['username'];
		$password = $_POST['password'];
		$tempUser = new User($username, $password);

	if ($tempId = $tempUser->checkPass()){
		$_SESSION['user_id'] = $tempId;
		$_SESSION['username'] = $username;
		echo "You are logged in as: <span class='username'> " . $_SESSION['username'] . "</span>";
		echo ' <a href="index.php">Home page</a>';
		die();
	} else { 
		echo "Bad password.";
		}
} 

?>

<form class="form-signin" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
	<fieldset>
		<label for="username" class="sr-only" >Username:</label>
		<input class="form-control" type="text" name="username" placeholder="Username" required autofocus><br />

		<label for="password" class="sr-only">Password:</label>
		<input class="form-control" type="password" name="password" placeholder="Password" required><br />

		<label for="verify">Verification:</label>
		<input type="text" name="verify" id="verify" placeholder="Enter captcha text.">
		<img class="captchaImg" src="php_class/captcha/captchacreator.php" alt="Verification passphrase"><br />

		<button name="submit" type="submit" class="btn btn-lg btn-primary btn-block">Login</button>

	</fieldset>
</form>







    	
  
<?php
require_once ('footer.php');
?>