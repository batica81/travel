<?php 
require_once 'php_class/dbAccess_class.php';

function showUserImages($user_id){

	$db = new DbAccess;
	$pdo = $db->getPDO();
	$stmt = $pdo->prepare("SELECT image_filename FROM image_data WHERE user_id = :user_id");

	try {
		$stmt->execute(array('user_id' => $user_id));
		$row = $stmt->fetch();

		// $row = $stmt->fetchAll();

		return $row['image_filename'];
		} 

	catch(Exception $e) {
  		echo 'Message: ' .$e->getMessage() . '<br />';
		}
} 