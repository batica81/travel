<?php 

require_once 'php_class/dbAccess_class.php';

function showUserImages($user_id){

	$db = new DbAccess;
	$pdo = $db->getPDO();
	$stmt = $pdo->prepare("SELECT image_id FROM image_data WHERE user_id = :user_id");

	try {
		$stmt->execute(array('user_id' => $user_id));
		$row = $stmt->fetchAll();
		return $row;
		} 

	catch(Exception $e) {
  		echo 'Message: ' .$e->getMessage() . '<br />';
		}
} // end showUserImages



function showImage($image_id){
	$db = new DbAccess;
	$pdo = $db->getPDO();
	$stmt = $pdo->prepare("SELECT image_filename FROM image_data WHERE image_id = :image_id");

	try {
		$stmt->execute(array('image_id' => $image_id));
		$row = $stmt->fetch();
		return $row['image_filename'];
		} 

	catch(Exception $e) {
  		echo 'Message: ' .$e->getMessage() . '<br />';
		}
} // end showImage




foreach (showUserImages(4571) as $key) {

	foreach ($key as $imggg) {

		echo showImage($imggg);
		echo '<img src="' . showImage($imggg) . '"/>' . '<br />';
	}
}
