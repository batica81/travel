<?php 
require_once ('header.php');
require_once ('php_class/dbaccess_class.php');

require_once ('show_user_images.php');


if (!isset($_SESSION['username'])) {
	echo "You are not logged in.";
	require_once ('footer.php');
	die();

} else {

	$username = $_SESSION['username'];
	$user_id = $_SESSION['user_id'];

	echo "Welcome " . $username . " ! This is your page!" . "<br />";

	echo "<img src='" . showUserImages($user_id) .  "'></img>";
	

	echo ' <a href="logout.php">Logout</a>' . "<br />";


}


if (isset($_POST['submit'])) {

	$image_description = trim($_POST['image_description']);

	$screenshotFilename = trim($_FILES['screenshot']['name']);

	if (!empty($screenshotFilename)) {

		$screenshot = time() . $_FILES['screenshot']['name'];
		$screenshotType = $_FILES['screenshot']['type'];
		$screenshotSize = $_FILES['screenshot']['size'];

			if (($screenshotType == 'image/jpeg') || ($screenshotType == 'image/pjpeg')
				|| ($screenshotType == 'image/gif') ||($screenshotType == 'image/png') 
				&& ($screenshotSize > 0)) {
		
				$target = './images/'.  $screenshot;

				move_uploaded_file($_FILES['screenshot']['tmp_name'], $target);
			
				$db = new DbAccess;
				$pdo = $db->getPDO();
				$stmt = $pdo->prepare("INSERT INTO image_data (user_id, image_description, image_filename, image_post_date) VALUES (:user_id, :image_description, :image_filename, NOW())");

					try {
					$stmt->execute(array('user_id' => $user_id, 'image_description' => $image_description, 'image_filename' => $target));

					echo "Image has been inserted into DB." . "<br />";
					} 

					catch(Exception $e) {
			  			echo 'Message: ' .$e->getMessage() . '<br />';
					}

			}
	}
}



?>

<p>Add tr profile.</p>

<form enctype="multipart/form-data" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">

	<input type="hidden" name="MAX_FILE_SIZE" value="655136"/>

	<label for="screenshot">Your image: </label>
	<input type="file" name="screenshot" id="screenshot"/><br />

	<label for="image_description">Describe the image: </label>
	<input type="text" name="image_description" placeholder="Enter image description"/><br/>

	<input type="submit" name="submit" value="submit"/><br/>

</form>

<?php
require_once ('footer.php');
?>